import requests
import json
import psycopg2 as postgres

conn = postgres.connect("dbname=testdb user=postgres password=password host=localhost port=5432")
cur = conn.cursor()
token = "U0rx6iHZYLFx1InrycvsfhYuxgRQPORyDM07f4Ekz2fHfftxJWAbIpzMD9SIl1sd"
base_url = "https://www.thebluealliance.com/api/v3"

event = "2018cars"

headers = {"X-TBA-Auth-Key": token}

r = requests.get(base_url + "/event/" + event + "/matches/simple", headers=headers)

results = json.loads(r.text)
print(r.text)
for match in results:
    SQL = "INSERT INTO match (match_num, match_level, match_set) VALUES (%s, %s, %s)"
    cur.execute(SQL, (match["match_number"], match["comp_level"], match["set_number"]))

conn.commit()