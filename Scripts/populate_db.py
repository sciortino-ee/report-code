import requests
import json
import psycopg2 as postgres

conn = postgres.connect("dbname=ee-db user=postgres password=firewire123 host=localhost port=5432")
cur = conn.cursor()
token = "U0rx6iHZYLFx1InrycvsfhYuxgRQPORyDM07f4Ekz2fHfftxJWAbIpzMD9SIl1sd"
base_url = "https://www.thebluealliance.com/api/v3"

year = 2018

event = "2018cars" # Champs
#event = "2018nyut" # Centrals
#event = "2018nyro" # Finger Lakes
headers = {"X-TBA-Auth-Key": token}

schema = "frc{}".format(year)

cur.execute("SET search_path TO {}".format(schema)) 

def populate_event(event):
    event_SQL = "INSERT INTO event (event_key, event_name, start_date, end_date) VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING"
    
    r = requests.get(base_url + "/event/" + event, headers=headers)
    
    result = json.loads(r.text)

    cur.execute(event_SQL, (
        event,
        result['name'],
        result['start_date'],
        result['end_date']
    ))



def populate_matches(event):
    match_SQL = "INSERT INTO match (event_key, match_num, match_level, match_set) VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING"

    r = requests.get(base_url + "/event/" + event + "/matches/simple", headers=headers)

    results = json.loads(r.text)

    for match in results:
        cur.execute(match_SQL, (
            event,
            match["match_number"], 
            match["comp_level"], 
            match["set_number"]
            ))

    print("Matches successfully populated")
    conn.commit()

def populate_teams(event):
    team_SQL = "INSERT INTO team (team_num, name, team_key) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING"
    team_event_SQL = "INSERT INTO event_team (event_key, team_num) VALUES (%s, %s) ON CONFLICT DO NOTHING"
    
    r = requests.get(base_url + "/event/" + event + "/teams/simple", headers=headers)

    results = json.loads(r.text)

    for team in results:
        cur.execute(team_SQL, (team["team_number"], team["nickname"], team["key"]))
        cur.execute(team_event_SQL, (event, team["team_number"]))

    print("Teams successfully populated")
    conn.commit()

def populate_match_teams(event):
    match_team_SQL = "CALL tba_add_team_match (%s,%s,%s,%s,%s,%s,%s);"

    r = requests.get(base_url + "/event/" + event + "/matches/simple", headers=headers)

    results = json.loads(r.text)
    for match in results:        
        # Populate matches by team
        # Blue teams
        for i in range(0,3):
            team = match['alliances']['blue']['team_keys'][i]
            cur.execute(match_team_SQL, (
                event,
                match['match_number'], 
                match['comp_level'], 
                match['set_number'], 
                team, 
                'blue', 
                i+1
                ))

        # Red teams
        for i in range(0,3): # 0 <= i <= 2
            team = match['alliances']['red']['team_keys'][i]
            cur.execute(match_team_SQL, (
                event,
                match['match_number'], 
                match['comp_level'], 
                match['set_number'], 
                team, 
                'red', 
                i+1 # Driver station
                ))
                
    print("Teams for each Match successfully populated")
    conn.commit()

def populate_match_results(event):
    match_result_SQL = "CALL tba_add_match_result (%s,%s, %s, %s, %s, %s, %s, %s);"

    r = requests.get(base_url + "/event/" + event + "/matches", headers=headers)
    results = json.loads(r.text)

    for match in results:
        # Put match results
        winner = parse_winner(match['winning_alliance'])
        cur.execute(match_result_SQL, (
            event,
            match['match_number'], 
            match['comp_level'], 
            match['set_number'], 
            winner, 
            match["alliances"]["blue"]["score"],
            match["alliances"]["red"]["score"],
            match["score_breakdown"]["blue"]["tba_gameData"]
            ))

    print("Match results successfully populated")
    conn.commit()

def parse_winner(winner):
    if winner == '':
        winner = 'tie'
    return winner

populate_event(event)
populate_teams(event)
populate_matches(event)
populate_match_teams(event)
populate_match_results(event)