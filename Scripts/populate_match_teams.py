import requests
import json
import psycopg2 as postgres

conn = postgres.connect("dbname=testdb user=postgres password=password host=localhost port=5432")
cur = conn.cursor()
token = "U0rx6iHZYLFx1InrycvsfhYuxgRQPORyDM07f4Ekz2fHfftxJWAbIpzMD9SIl1sd"
base_url = "https://www.thebluealliance.com/api/v3"

event = "2018cars"

headers = {"X-TBA-Auth-Key": token}

r = requests.get(base_url + "/event/" + event + "/matches/simple", headers=headers)

results = json.loads(r.text)
print(r.text)
for match in results:
    matchInfo = match["match_number"], match["comp_level"], match["set_number"]
    # Populate matches by team
    SQLMT = '''
    INSERT INTO team_match 
        (match_id, team_num, driver_station, alliance)
    VALUES (
        (SELECT match_id as id FROM match m where m.match_num = %s AND m.match_level = %s AND m.match_set = %s),
        (SELECT team_num as num FROM team t where t.team_key = %s),
        %s,
        %s
        ) ON CONFLICT(match_id, team_num) DO NOTHING
    '''
    for i in range(0,3): 
        team = match['alliances']['blue']['team_keys'][i]
        cur.execute(SQLMT, (match['match_number'], match['comp_level'], match['set_number'], team, i+1, 'blue'))
    for i in range(0,3): 
        team = match['alliances']['red']['team_keys'][i]
        cur.execute(SQLMT, (match['match_number'], match['comp_level'], match['set_number'], team, i+1, 'red'))

    # Populate match results
    SQLMR = '''
    INSERT INTO match_result
        (match_id, winner, blue_points, red_points)
    VALUES (
        (SELECT match_id FROM match m WHERE m.match_num = %s AND m.match_level = %s AND m.match_set = %s),
        %s,
        %s,
        %s
    ) ON CONFLICT(match_id) DO NOTHING
    '''
    winner = match['winning_alliance']
    if winner == '':
        winner = 'tie'
        print('Tie on match ' + str(match['match_number']))
    print(winner)
    cur.execute(SQLMR, (match['match_number'], match['comp_level'], match['set_number'], winner, match["alliances"]["blue"]["score"], match["alliances"]["red"]["score"]))
conn.commit()