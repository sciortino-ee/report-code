import requests
import json
import psycopg2 as postgres

conn = postgres.connect("dbname=testdb user=postgres password=password host=localhost port=5432")
cur = conn.cursor()
token = "U0rx6iHZYLFx1InrycvsfhYuxgRQPORyDM07f4Ekz2fHfftxJWAbIpzMD9SIl1sd"
base_url = "https://www.thebluealliance.com/api/v3"

event = "2018cars"

headers = {"X-TBA-Auth-Key": token}

r = requests.get(base_url + "/event/" + event + "/teams/simple", headers=headers)

results = json.loads(r.text)

print(results[1]["name"])

for team in results:
    SQL = "INSERT INTO team (team_num, name, team_key) VALUES (%s, %s, %s)"
    cur.execute(SQL, (team["team_number"], team["nickname"], team["key"]))
    print(SQL, (team["team_number"], team["nickname"]))

conn.commit()