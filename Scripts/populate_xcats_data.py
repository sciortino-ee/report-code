import pandas as pd
import numpy as np
import psycopg2 as psql
import os

class CubeData:
    def __init__(self, successful, failed):
        self.successful = successful
        self.failed = failed
        self.total_attempts = successful + failed

def psql_connect():
    password = os.environ.get('EE_DB_PASSWORD')
    password = "firewire123"
    db_connection = psql.connect("dbname=ee_db user=alfs password={} host=10.0.1.83 port=5432".format(password))
    db_cursor = db_connection.cursor()

    db_cursor.execute("SET search_path TO {}".format("frc2018"))
    return db_cursor, db_connection

def main():
    eliminated_points = 0

    filename = os.environ.get('EE_CSV_PATH')
    print(filename)
    data = pd.read_csv(filename)

    data.rename(inplace=True, columns={'TEAM_TEAM_NUM': 'TEAM_NUM'})

    data.drop(
        columns=['SCOUT_NAME', 'ALLIANCE', 'DRIVER_POS'],
        inplace=True
    )

    # Define SQL Queries
    add_match_info_sql = '''
    INSERT INTO x_match_data 
        (match_id, team_num, robot_pos) 
    VALUES 
        (%s, %s, %s)
    '''
    add_auto_data_sql = '''
    INSERT INTO x_auto_data 
        (match_id, team_num, baseline_cross, cubes_scale, f_cubes_scale, cubes_switch, f_cubes_switch) 
    VALUES 
        (%s, %s, %s, %s, %s, %s, %s)
    '''
    add_teleop_data_sql = '''
    INSERT INTO x_teleop_data 
        (
        match_id, 
        team_num, 
        cubes_scale, 
        f_cubes_scale, 
        cubes_switch, 
        f_cubes_switch, 
        cubes_opp_switch, 
        f_cubes_opp_switch
        ) 
    VALUES 
        (%s, %s, %s, %s, %s, %s, %s, %s)
    '''
    add_endgame_data_sql = '''
    INSERT INTO x_endgame_data
        (match_id, team_num, climb, climb_assist)
    VALUES
        (%s, %s, %s, %s)
    '''
    for index, row in data.iterrows():
        db_cursor, conn = psql_connect()

        team_number = row['TEAM_NUM']
        match_number = row['MATCH_NUM']

        db_cursor.callproc("get_match_id", ("2018cars", match_number, "qm", 1))

        match_id = db_cursor.fetchone()[0];

        robot_pos = row['ROBOT_POS'] + 1

        # Get Autonomous Values
        baseline = True if row['BASELINE_CROSS'] == 1 else False
        auto_cubes_scale = CubeData(row['AUTO_CUBES_SCALE'], row['AUTO_CUBES_SCALE_FAIL'])
        auto_cubes_switch = CubeData(row['AUTO_CUBES_SWITCH'], row['AUTO_CUBES_SWITCH_FAIL'])

        # Get Teleoperated Values
        cubes_scale = CubeData(row['CUBES_SCALE'], row['CUBES_SCALE_FAIL'])
        cubes_switch = CubeData(row['CUBES_SWITCH'], row['CUBES_SWITCH_FAIL'])
        cubes_opp_switch = CubeData(row['CUBES_OPP_SWITCH'], row['CUBES_OPP_SWITCH_FAIL'])

        # Get Endgame Values
        climb = row['CLIMB']
        climb_assist = True if row['CLIMBS_ASSISTED'] > 0 else False

        try:
            # Generate Match Info
            db_cursor.execute(add_match_info_sql, (
                match_id,
                team_number,
                robot_pos
            ))

            # Generate Autonomous Info
            db_cursor.execute(add_auto_data_sql, (
                match_id,
                team_number,
                baseline,
                auto_cubes_scale.successful,
                auto_cubes_scale.failed,
                auto_cubes_switch.successful,
                auto_cubes_switch.failed
            ))

            # Generate Teleop Info
            db_cursor.execute(add_teleop_data_sql, (
                match_id,
                team_number,
                cubes_scale.successful,
                cubes_scale.failed,
                cubes_switch.successful,
                cubes_switch.failed,
                cubes_opp_switch.successful,
                cubes_opp_switch.failed
            ))

            # Generate Endgame Data
            db_cursor.execute(add_endgame_data_sql, (
                match_id,
                team_number,
                climb,
                climb_assist
            ))
        except psql.IntegrityError as e:
            print("Error for match {}, team {}".format(match_number, team_number))
            eliminated_points += 1
            print(e)
        finally:
            db_cursor.close()
        conn.commit()
    print("Eliminated Points: {}".format(eliminated_points))
main()