import networklib as lib
import matplotlib.pyplot as plt
import numpy as np
import json
import time

train_data = np.loadtxt('mnist_train.csv', delimiter=',')
test_data = np.loadtxt('mnist_test.csv', delimiter=',')

fac = 255  *0.99 + 0.01
train_imgs = np.asfarray(train_data[:, 1:]) / fac
test_imgs = np.asfarray(test_data[:, 1:]) / fac
train_labels = np.asfarray(train_data[:, :1])
test_labels = np.asfarray(test_data[:, :1])

lr = np.arange(10)
train_labels_one_hot = (lr==train_labels).astype(np.float)
test_labels_one_hot = (lr==test_labels).astype(np.float)

train_labels_one_hot[train_labels_one_hot==0] = 0.01
train_labels_one_hot[train_labels_one_hot==1] = 0.99
test_labels_one_hot[test_labels_one_hot==0] = 0.01
test_labels_one_hot[test_labels_one_hot==1] = 0.99

input_layer_size = len(train_imgs[0])
output_layer_size = len(train_labels_one_hot[0])

hidden_layers = [10,10]

network = lib.generate_network(0.02, input_layer_size, output_layer_size, hidden_layers)

training_accuracy = []
testing_accuracy = []

x = []
y1 = []
y2 = []

epochs = 1

for k in range (0, epochs):
    print("Epoch {}".format(k))
    for i in range(0, len(train_imgs), 1000):
        tr, ts = network.train_batch(train_imgs[i:i+1000], train_labels_one_hot[i:i+1000], test_imgs[0:1000], test_labels[0:1000])
        training_accuracy.append(tr)
        testing_accuracy.append(ts)
        print("Data points {} through {}".format(i, i+1000))

        x.append((i / 1000) + k * 60)
        y1.append(tr[1] / tr[2])
        y2.append(ts[1] / ts[2])

        print("Training Loss: {}\nTesting Loss: {}".format(tr[1] / tr[2], ts[1] / ts[2]))

plt.plot(x,y1,'r-', label='Training Loss')
plt.plot(x,y2,'b-', label='Testing Loss')
plt.ylim(0,1)
plt.legend()
plt.xlabel("Batch")
plt.ylabel("Loss")
plt.show()