import numpy as np
from abc import ABC, abstractmethod


@np.vectorize
def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))


@np.vectorize
def sigmoid_derivative(x):
    sigm = sigmoid(x)
    return sigm * (1 - sigm)


@np.vectorize
def error(y, a):  # C = \frac{1}{2}(y - a)^2
    return 1/2 * (y - a)


@np.vectorize
def error_derivative(y, a):  # \frac{\partial C}{\partial a} = a - y
    return a - y


class Layer(ABC):
    def __init__(self, size):
        self.size = size
        self.input_size = size

        self.a = np.zeros(size, 1)

    def calculate(self, prev_a):
        if len(prev_a) != self.input_size:
            raise ValueError("Length of input array not equal to number of neurons in layer")
        self.a = self._calculate(prev_a)
        return self.a

    @abstractmethod
    def _calculate(self, prev_a):
        pass


class InputLayer(Layer):
    def _calculate(self, prev_a):
        return prev_a


class WeightedLayer(Layer, ABC):
    def __init__(self, size, input_size, activation_function, activation_function_derivative):
        super(WeightedLayer, self).__init__(size)
        self.input_size = input_size

        self.activation_function = activation_function
        self.activation_function_derivative = activation_function_derivative

        self.w = np.random.randn(size, self.input_size)
        self.b = np.random.randn(size, 1)
        self.z = np.zeros(size, 1)  # = aw + b

        self.delta = np.zeros(np.shape(self.b))
        self.delta_w = np.zeros(np.shape(self.w))
        self.delta_b = np.zeros(np.shape(self.b))
        self.n = 0

    def _calc_z(self, prev_a):
        self.z = np.dot(prev_a, self.w) + self.b
        return self.z

    def _calculate(self, prev_a):
        z = self._calc_z(prev_a)
        return self.activation_function(z)

    @abstractmethod
    def get_cost(self):
        pass

    def get_delta(self, label):
        self.delta = np.multiply(self.get_cost(), self.activation_function_derivative(self.z))
        return self.delta

    def new_batch(self):
        self.w = self.w + self.delta_w / self.n
        self.b = self.b + self.delta_b / self.n

        self.delta_b = np.zeros(np.shape(self.b))
        self.delta_w = np.zeros(np.shape(self.w))

        self.n = 0

    def update_layer(self, batch):
        self._update_weights(batch)
        self._update_biases(batch)
        if batch:
            self.n += 1

    def _update_weights(self, batch):
        if batch:
            self.delta_w = self.delta_w + np.dot(self.delta, self.w)
        else:
            self.delta_w = np.dot(self.delta, self.w)
            self.w = self.w + self.delta_w

    def _update_biases(self, batch):
        if batch:
            self.delta_b = self.delta_b + self.delta
        else:
            self.delta_b = self.delta
            self.b = self.b + self.delta_b


class OutputLayer(WeightedLayer):
    def __init__(self, size, input_size, activation_function, activation_function_derivative, cost_function):
        super(OutputLayer, self).__init__(size, input_size, activation_function, activation_function_derivative)
        self.cost_function = cost_function

    def get_cost(self, label):
        return self.cost_function(label, self.z)


class InnerLayer(WeightedLayer):
    def __init__(self, size, input_size, activation_function, activation_function_derivative, next_layer):
        super(InnerLayer, self).__init__(size, input_size, activation_function, activation_function_derivative)
        self.next_layer = next_layer

    def get_cost(self):
        return np.dot(np.transpose(self.next_layer.w), self.next_layer.delta)

