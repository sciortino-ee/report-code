import numpy as np

np.random.seed(0)


@np.vectorize
def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))


@np.vectorize
def sigmoid_derivative(x):
    sigm = sigmoid(x)
    return sigm * (1 - sigm)


@np.vectorize
def error(y, a):  # C = \frac{1}{2}(y - a)^2
    return 1/2 * (y - a)


@np.vectorize
def error_derivative(y, a):  # \frac{\partial C}{\partial a} = a - y
    return a - y


def test_index_equality(x, y):
    guess = np.argmax(x)
    label = np.argmax(y)
    return True if guess == label else False


class Layer:
    def __init__(self, size, input_size):
        self.size = size
        self.input_size = input_size

        self.delta_count = 0

        # Initialize Forward prop variables
        self.w = np.random.randn(size, self.input_size)
        self.b = np.random.randn(size, 1)
        self.z = np.zeros(size, 1) # = aw + b
        self.a = np.zeros(size, 1)

        # Initialize backprop variables
        self.delta = np.zeros(np.shape(self.b))
        self.delta_w = np.zeros(np.shape(self.w))
        self.delta_b = np.zeros(np.shape(self.b))

    def _calc_z(self, prev_a):
        self.z = np.dot(prev_a, self.w) + self.b
        return self.z

    def _calc_a(self):
        self.a = sigmoid(self.z)
        return self.a

    def _calculate(self, prev_a):
        prev_a.shape = (prev_a.shape[0], 1) # Make sure that previous value is shaped correctly
        self._calc_z(prev_a)
        out = self._calc_a()
        return out

    def calculate(self, x):
        if len(x) != self.input_size:
            raise ValueError("Length of input array not equal to number of neurons in layer")
        self.a = self._calculate(x)
        return self.a

    def update_deltas(self, delta_b, delta_w):
        self.delta_w = self.delta_w + delta_w
        self.delta_b = self.delta_b + delta_b
        self.delta_count += 1

    def update_layer(self, learning_rate):
        d_weights = learning_rate * self.delta_w / self.delta_count
        d_biases = learning_rate * self.delta_b / self.delta_count
        self.w = self.w + d_weights
        self.b = self.b + d_biases
        self.delta_count = 0

    def get_delta(self, next_weights, next_error):
        return np.dot(next_weights.transpose(), next_error) * sigmoid_derivative(self.weighted_input)


class OutputLayer(Layer):
    def __init__(self, size, input_size):
        super().__init__(size, input_size)

    def get_delta(self, label):
        label.shape = (label.shape[0], 1)
        delta = error_derivative(self.output, label)
        delta = delta * sigmoid_derivative(self.weighted_input)
        return delta

    def calculate(self, prev_a):
        out = self._calculate(prev_a)
        return out


class InputLayer(Layer):
    def __init__(self, size):
        self.size = size

    def _calculate(self, input):



class Network:
    def __init__(self, learning_rate, layers, output_layer):
        # Set the learning rate according to the constructor
        self.learning_rate = learning_rate

        # Create a "private" array of all layers
        self._layers = layers
        # Add the output layer to the private layer array
        self._layers.append(output_layer)
        # Save the number of layers in the full-layer array to a local variable
        self.num_layers = len(self._layers)

        # Create a "public" array of layers that eliminates the input layer
        self.layers = self._layers[1:]

        self.output = []
        self.accuracy_batch = []

    def feed_forward(self, input_data):
        # Set the output to the input data, so it is passed into the input layer
        self.output = input_data

        # Iterate over all the layers
        for i in range(0, self.num_layers):
            # Calculate the output based on the output of the previous layer,
            # and store it to use in the next layer
            self.output = self._layers[i].calculate(self.output)

        # Return the output of the last layer
        return self.output

    def backprop(self, label):
        # Iterate over each layer, but backwards
        for i in range(1, self.num_layers):
            # Initialize delta arrays for weights and for biases
            delta_w = np.zeros(())
            delta_b = []

            # If we're on the output layer (last layer, index -1), get the error gradient using
            # the appropriate function for the output layer
            if -i == -1:
                delta = self._layers[-i].get_delta(label)
            # Otherwise get the error gradient according to the general function for all layers
            else:
                # Get the delta based on the preceding layers' weights and the previous layers'
                # delta
                delta = self._layers[-i].get_delta(
                    self._layers[-i + 1].weights,
                    delta
                )

            # Change in biases is just the delta
            delta_b = delta
            # Change in the weights is the dot product of the delta and the next layers' output
            delta_w = np.dot(delta, self._layers[-i - 1].output.transpose())

            # Update the deltas of the current layer with the two new calculated deltas
            self.layers[-i].update_deltas(delta_b, delta_w)

    def update_layers(self):
        for l in self.layers:
            l.update_layer(self.learning_rate)
            l.init_deltas()

    def train_batches(self, data, labels, batch_size=25):
        self.accuracy_batch = []
        batched_data = []
        batched_labels = []
        for i in range(0, len(data), batch_size):
            batched_data.append(data[i:i + batch_size])
            batched_labels.append(labels[i:i + batch_size])
        batch = 1
        for d, l in zip(batched_data, batched_labels):
            batch_correct = 0
            if batch % 10 == 0:
                print("Batch {} of {}".format(batch, len(batched_data)))
            for data, label in zip(d, l):
                self.feed_forward(data)
                self.backprop(label)
                if test_index_equality(self.output, label):
                    batch_correct += 1
            batch += 1
            self.accuracy_batch.append(batch_correct / batch_size)
            self.update_layers()

    def train_batch(self, data, labels, test_data, test_labels):
        correct = 0
        incorrect = 0
        total = 0
        for d, l in zip(data, labels):
            self.feed_forward(d)
            d = np.argmax(l)
            if test_index_equality(self.output, d):
                correct += 1
            else:
                incorrect += 1
            total += 1
            self.backprop(l)
        t_correct = 0
        t_incorrect = 0
        t_total = 0
        for d, l in zip(test_data, test_labels):
            output = self.feed_forward(d)
            if test_index_equality(output, l):
                t_correct += 1
            else:
                t_incorrect += 1
            t_total += 1

        return (correct, incorrect, total), (t_correct, t_incorrect, t_total)

    def train(self, data, label):
        for d, l in zip(data, label):
            self.feed_forward(d)
            self.backprop(l)


def generate_network(learning_rate, input_dimension, output_dimension, hidden_dimensions=[]):
    input_layer = InputLayer(input_dimension)
    hidden_layers = []
    prev_dim = input_dimension
    if len(hidden_dimensions) > 0:
        for i in range(0, len(hidden_dimensions)):
            hidden_layers.append(Layer(hidden_dimensions[i], prev_dim))
            prev_dim = hidden_dimensions[i]
    output_layer = OutputLayer(output_dimension, prev_dim)
    layers = [input_layer]
    layers.extend(hidden_layers)
    return Network(learning_rate, layers, output_layer)
