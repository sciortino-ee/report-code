import numpy as np


def load_data(train_filename, test_filename):
    train_data = np.loadtxt(train_filename, delimiter=',')
    test_data = np.loadtxt(test_filename, delimiter=',')

    fac = 255 * 0.99 + 0.01
    train_imgs = np.asfarray(train_data[:, 1:]) / fac
    test_imgs = np.asfarray(test_data[:, 1:]) / fac

    train_labels = np.asfarray(train_data[:, :1])
    test_labels = np.asfarray(test_data[:, :1])

    lr = np.arange(10)

    # transform labels into one hot representation
    train_labels_one_hot = (lr == train_labels).astype(np.float)
    test_labels_one_hot = (lr == test_labels).astype(np.float)

    # we don't want zeroes and ones in the labels either:
    train_labels_one_hot[train_labels_one_hot == 0] = 0.01
    train_labels_one_hot[train_labels_one_hot == 1] = 0.99

    test_labels_one_hot[test_labels_one_hot == 0] = 0.01
    test_labels_one_hot[test_labels_one_hot == 1] = 0.99

    return test_imgs, test_labels_one_hot, train_imgs, train_labels_one_hot
