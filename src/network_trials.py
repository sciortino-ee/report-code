from eelib import network as lib
import numpy as np
import json
import time


def test_network(layers, num_trials, layer_size=100):
    hidden_dims = [layer_size] * layers
    trials = []
    for i in range(0, num_trials):
        network = lib.generate_network(1, input_layer_size, output_layer_size, hidden_dims)

        start_time = time.time()
        network.train(train_imgs, train_labels_one_hot)
        end_time = time.time()

        correct = 0
        incorrect = 0
        total = 0
        for d, l in zip(test_imgs, test_labels):
            total += 1
            result = network.feed_forward(d)
            guess = np.argmax(result)
            if guess == l[0]:
                correct += 1
            else:
                incorrect += 1
        trials.append({
            "time": end_time - start_time,
            "percent_accuracy": (correct / total) * 100
        })

    data = {
        "layers": layers,
        "layer_size": layer_size,
        "trials": trials
    }
    return data


    print("{} Layers:".format(layers))
    print("\tPercent Correct: {} \%".format(100 * (correct / total)))
    print("\tTraining Time: {} m".format((end_time - start_time) / 60))

train_data = np.loadtxt('mnist_train.csv', delimiter=',')
test_data = np.loadtxt('mnist_test.csv', delimiter=',')

fac = 255  *0.99 + 0.01
train_imgs = np.asfarray(train_data[:, 1:]) / fac
test_imgs = np.asfarray(test_data[:, 1:]) / fac
train_labels = np.asfarray(train_data[:, :1])
test_labels = np.asfarray(test_data[:, :1])

hidden_dims = [100, 100]

lr = np.arange(10)
# transform labels into one hot representation
train_labels_one_hot = (lr==train_labels).astype(np.float)
test_labels_one_hot = (lr==test_labels).astype(np.float)
# we don't want zeroes and ones in the labels neither:
train_labels_one_hot[train_labels_one_hot==0] = 0.01
train_labels_one_hot[train_labels_one_hot==1] = 0.99
test_labels_one_hot[test_labels_one_hot==0] = 0.01
test_labels_one_hot[test_labels_one_hot==1] = 0.99

input_layer_size = len(train_imgs[0])
output_layer_size = len(train_labels_one_hot[0])

data = []

for i in range(0, 11):
    data.append(test_network(i, 5))

with open('data.json', 'w') as outfile:
    json.dump(data, outfile)