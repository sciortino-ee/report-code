from eelib import network as lib
from eelib import utils as utils
import matplotlib.pyplot as plt

testing_data, testing_labels, training_data, training_labels \
    = utils.load_data('data/mnist_train.csv', 'data/mnist_test.csv')
print("Data Loaded - Commencing Training")

# Network generation values
input_layer_size = len(training_data[0])
output_layer_size = len(training_labels[0])
learning_rate = 2
hidden_layers = [5, 5]

network = lib.generate_network(learning_rate, input_layer_size, output_layer_size, hidden_layers)

data = []

network.train_batches(training_data, training_labels)

total_correct = 0

for d,l in zip(testing_data, testing_labels):
    out = network.feed_forward(d)
    if lib.test_index_equality(out, l):
        total_correct += 1

print(total_correct / len(testing_data))

plt.plot(network.accuracy_batch)
plt.ylabel("Accuracy")
plt.show()
