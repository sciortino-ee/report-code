-- SELECT * FROM (
-- SELECT 
--     m.match_num, tm.team_num, tm.alliance, (tm.alliance = mr.winner) AS winner
-- FROM team_match tm 
-- NATURAL JOIN match m 
-- NATURAL JOIN match_result mr
-- WHERE
--     m.match_level='qm' 
--     AND m.event_key='2018cars'
-- ) mi JOIN (
-- SELECT
--     m.match_num, xtd.team_num, AVG(cubes_scale) AS a_cubes_scale, AVG(cubes_switch) AS a_cubes_switch, AVG(cubes_opp_switch) AS a_cubes_opp_switch
-- FROM x_teleop_data xtd
-- NATURAL JOIN match m
-- WHERE
--     m.match_num <= mi.match_num
-- GROUP BY xtd.team_num
-- ) ts ON 
-- mi.team_num = ts.team_num
-- WHERE mi.match_num = 30
-- ;
CREATE OR REPLACE VIEW ee_alliance_data AS (
    SELECT
        mavg.match_num
    ,   mavg.alliance
    ,   mavg.scale_avg as scale_avg
    ,   mavg.switch_avg as switch_avg
    ,   mavg.opp_switch_avg as opp_switch_avg
    ,   mavg.auto_scale_avg as auto_scale_avg
    ,   mavg.auto_switch_avg as auto_switch_avg
    ,   CASE winner WHEN TRUE THEN 1 ELSE 0 END as win
    FROM (
        SELECT DISTINCT
            m.match_num
        ,   tm.alliance
        ,   SUM(avgs.scale_avg) OVER w as scale_avg
        ,   SUM(avgs.switch_avg) OVER w as switch_avg
        ,   SUM(avgs.opp_switch_avg) OVER w as opp_switch_avg
        ,   SUM(avgs.auto_scale_avg) OVER w as auto_scale_avg
        ,   SUM(avgs.auto_switch_avg) OVER w as auto_switch_avg
        ,   CASE tm.alliance WHEN mr.winner THEN TRUE ELSE FALSE END as winner
        ,   COUNT(*) OVER c as team_count
        FROM (
            SELECT
                xtd.match_id AS match_id
            ,   xtd.team_num AS team_num
            ,   AVG(xtd.cubes_scale) OVER w AS scale_avg
            ,   AVG(xtd.cubes_switch) OVER w AS switch_avg
            ,   AVG(xtd.cubes_opp_switch) OVER w AS opp_switch_avg
            ,   AVG(xad.cubes_scale) OVER w AS auto_scale_avg
            ,   AVG(xad.cubes_switch) OVER w AS auto_switch_avg
            FROM 
                x_teleop_data xtd
            JOIN x_auto_data xad ON
                    xad.match_id = xtd.match_id
                AND xad.team_num = xtd.team_num
            WINDOW w AS (PARTITION BY xtd.team_num ORDER BY xtd.match_id)
        ) avgs
        NATURAL JOIN match m
        NATURAL JOIN team_match tm
        NATURAL JOIN match_result mr
        WHERE 
                m.event_key = '2018cars'
            AND m.match_level = 'qm'
        WINDOW 
            w AS (PARTITION BY avgs.match_id, tm.alliance)
        ,   c AS (PARTITION BY avgs.match_id)
        ORDER BY 
            m.match_num
        ,   tm.alliance 
    ) mavg
    WHERE mavg.team_count = 6
);

CREATE OR REPLACE VIEW ee_data AS (
SELECT 
    rd.scale_avg as red_scale_avg
,   rd.switch_avg as red_switch_avg
,   rd.opp_switch_avg as red_opp_switch_avg
,   rd.auto_scale_avg as red_auto_scale_avg
,   rd.auto_switch_avg as red_auto_switch_avg
,   bd.scale_avg as blue_scale_avg
,   bd.switch_avg as blue_switch_avg
,   bd.opp_switch_avg as blue_opp_switch_avg
,   bd.auto_scale_avg as blue_auto_scale_avg
,   bd.auto_switch_avg  as blue_auto_switch_avg
,   rd.win as red_win
,   bd.win as blue_win
FROM (
    SELECT * FROM ee_alliance_data ed WHERE ed.alliance = 'red'
) rd
JOIN (
    SELECT * FROM ee_alliance_data ed WHERE ed.alliance = 'blue'
) bd ON
rd.match_num = bd.match_num
);
