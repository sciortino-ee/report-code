-- Current primary key on match_id, team_num. For larger system (multi-user, multi-team) add a username constraint
CREATE TABLE x_match_data (
    match_id            integer REFERENCES match (match_id)
,   team_num            integer REFERENCES team (team_num)
,   robot_pos           integer CHECK (robot_pos > 0 AND robot_pos <= 3)
,   PRIMARY KEY (match_id, team_num)
,   CONSTRAINT match_team_alliance FOREIGN KEY (match_id, team_num) REFERENCES team_match (match_id, team_num)
);

CREATE TABLE x_auto_data (
    match_id        integer
,   team_num        integer
,   baseline_cross  boolean
,   cubes_scale     integer
,   f_cubes_scale   integer
,   cubes_switch    integer
,   f_cubes_switch  integer
,   FOREIGN KEY (match_id, team_num) REFERENCES x_match_data (match_id, team_num)
,   PRIMARY KEY (match_id, team_num)
);

CREATE TABLE x_reported_game_data (
    match_id    integer
,   team_num    integer
,   game_string varchar NOT NULL
,   CONSTRAINT game_string_check CHECK (game_string ~ '(?=[RL]{3})(?=^\w{3}$)') -- Check that the string only contains letters R and L, and is only 3 characters long
,   FOREIGN KEY (match_id, team_num) REFERENCES x_match_data (match_id, team_num)
,   PRIMARY KEY (match_id, team_num)
);

CREATE TABLE x_teleop_data (
    match_id            integer
,   team_num            integer
,   cubes_scale         integer
,   f_cubes_scale       integer
,   cubes_switch        integer
,   f_cubes_switch      integer
,   cubes_opp_switch    integer
,   f_cubes_opp_switch  integer
,   CONSTRAINT match_team FOREIGN KEY (match_id, team_num) REFERENCES x_match_data (match_id, team_num)
,   PRIMARY KEY (match_id, team_num)
);

CREATE TABLE x_endgame_data (
    match_id            integer
,   team_num            integer
,   climb               boolean
,   climb_assist        boolean -- integer CHECK climb_assist > 0 AND climb_assist < 3
,   CONSTRAINT match_team FOREIGN KEY (match_id, team_num) REFERENCES x_match_data (match_id, team_num)
,   PRIMARY KEY (match_id, team_num)
)

