-- 2018-specific tables
SET search_path TO frc2018;
-- Create table for game specific data
CREATE TABLE game_specific_data (
    match_id    integer REFERENCES match_result (match_id) PRIMARY KEY
,   game_string varchar NOT NULL
,   CONSTRAINT game_string_check CHECK (game_string ~ '(?=[RL]{3})(?=^\w{3}$)') -- Check that the string only contains letters R and L, and is only 3 characters long
);