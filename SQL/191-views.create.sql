CREATE OR REPLACE VIEW team_stats AS (
    SELECT team_num, wins, losses, ties, (wins + losses + ties) as total_matches 
    FROM (
        SELECT w.team_num as team_num, w.wins as wins, l.losses as losses, (CASE WHEN t.ties > 0 THEN t.ties ELSE 0 END) as ties
        FROM (
            SELECT
            tm.team_num AS team_num,
            COUNT(*) AS wins
            FROM
            team_match tm
            JOIN match_result mr ON tm.match_id = mr.match_id
            AND tm.alliance = mr.winner
            GROUP BY
            tm.team_num
        ) w
        LEFT OUTER JOIN (
            SELECT
            tm.team_num AS team_num,
            COUNT(*) AS losses
            FROM
            team_match tm
            JOIN match_result mr ON tm.match_id = mr.match_id
            AND tm.alliance != mr.winner
            AND mr.winner != 'tie'
            GROUP BY
            tm.team_num
        ) l ON w.team_num = l.team_num
        LEFT OUTER JOIN (
            SELECT
            tm.team_num AS team_num,
            COUNT(*) AS ties
            FROM
            team_match tm
            JOIN match_result mr ON tm.match_id = mr.match_id
            AND mr.winner = 'tie'
            GROUP BY
            tm.team_num
        ) t ON w.team_num = t.team_num
    ) s ORDER BY team_num
);