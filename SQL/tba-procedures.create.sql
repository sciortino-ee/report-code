SET search_path TO frc2018;

CREATE OR REPLACE FUNCTION get_match_id (
    v_event_key     IN varchar
,   v_match_num     IN integer
,   v_match_level   IN varchar
,   v_match_set     IN integer
)
RETURNS integer AS 
$$
    DECLARE
        match_id integer;
    BEGIN
        SELECT m.match_id INTO match_id FROM match m WHERE 
            (m.match_level = v_match_level) AND 
            (m.match_num = v_match_num) AND 
            (m.match_set = v_match_set) AND
            (m.event_key = v_event_key);
        RETURN match_id;
    END; 
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_team_num_by_key (v_team_key varchar) RETURNS integer AS
$$
    DECLARE
        team_num integer;
    BEGIN  
        SELECT t.team_num INTO team_num FROM team t WHERE t.team_key = v_team_key;
        RETURN team_num;
    END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE tba_add_match_result (
    event_key   IN varchar
,   match_num   IN integer
,   match_level IN varchar
,   match_set   IN integer
,   winner      IN varchar
,   red_points  IN integer
,   blue_points IN integer
,   game_data   IN varchar
)
LANGUAGE plpgsql
AS
$$
DECLARE
    v_match_id integer;
BEGIN
    SELECT get_match_id(event_key, match_num, match_level, match_set) INTO v_match_id;
    INSERT INTO match_result(match_id, winner, red_points, blue_points) 
    VALUES (v_match_id, winner, red_points, blue_points)
    ON CONFLICT (match_id) DO NOTHING;
    INSERT INTO game_specific_data (match_id, game_string) VALUES (v_match_id, game_data) ON CONFLICT (match_id) DO NOTHING;
END;
$$;

CREATE OR REPLACE PROCEDURE tba_add_team_match (
    event_key       IN varchar
,   match_num       IN integer
,   match_level     IN varchar
,   match_set       IN integer
,   team_key        IN varchar
,   alliance        IN varchar
,   driver_station  IN integer
) 
LANGUAGE plpgsql
AS 
$$
DECLARE
    v_match_id integer;
    v_team_num integer;
BEGIN
    SELECT get_match_id(event_key, match_num, match_level, match_set) INTO v_match_id;
    SELECT get_team_num_by_key(team_key) INTO v_team_num;
    INSERT INTO team_match(match_id, team_num, alliance, driver_station) 
        VALUES (v_match_id, v_team_num, alliance, driver_station) 
        ON CONFLICT (match_id, team_num) DO NOTHING;
END;
$$;